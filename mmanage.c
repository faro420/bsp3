/* Description: Memory Manager BSP3*/
/* Prof. Dr. Wolfgang Fohl, HAW Hamburg */
/* Sommer 2015
 * 
 * This is the memory manager process that
 * works together with the vmaccess process to
 * mimic virtual memory management.
 *
 * The memory manager process will be invoked
 * via a SIGUSR1 signal. It maintains the page table
 * and provides the data pages in shared memory
 *
 * This process is initiating the shared memory, so
 * it has to be started prior to the vmaccess process
 *
 * TODO:
 * currently nothing
 * */

#include "mmanage.h"
#include "vmem.h"
 
#define PAGEFILE_OPEN_MODE "wb+"
#define LOGFILE_OPEN_MODE "w"

struct vmem_struct *vmem = NULL;
FILE *pagefile = NULL;
FILE *logfile = NULL;
int signal_number = 0;          


int main(void){
    struct sigaction sigact;

    init_pagefile(MMANAGE_PFNAME);
    
    if(!pagefile) {
        perror("creating pagefile failed");
        exit(EXIT_FAILURE);
    }

    logfile = fopen(MMANAGE_LOGFNAME, "w");
    
    if(!logfile) {
        perror("creating logfile failed");
        exit(EXIT_FAILURE);
    }

    vmem_init();
    
    if(!vmem) {
        perror("Error initialising vmem");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "vmem created successfully\n");
    }
#endif 

    /* Setup signal handler */
    /* Handler for USR1 */
    sigact.sa_handler = sighandler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    if(sigaction(SIGUSR1, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR1");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR1 handler installed successfully\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGUSR2, &sigact, NULL) == -1) {
        perror("Error installing signal handler for USR2");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "USR2 handler installed successfully\n");
    }
#endif /* DEBUG_MESSAGES */

    if(sigaction(SIGINT, &sigact, NULL) == -1) {
        perror("Error installing signal handler for INT");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "INT handler installed successfully\n");
    }
#endif /* DEBUG_MESSAGES */

    /* signal processor*/
    while(1) {
        signal_number = 0;
        pause();
        if(signal_number == SIGUSR1) {  /* Page fault */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR1\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGUSR2) {     /* PT dump */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGUSR2\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        }
        else if(signal_number == SIGINT) {   
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "Processed SIGINT\n");
#endif /* DEBUG_MESSAGES */
        }
    }

    return 0;
}

/* Your code goes here... */

void sighandler(int signal){
    switch(signal){

       case SIGUSR1:
            allocate_page();
            break;

       case SIGUSR2:
            break;

       case SIGINT:
            /*user exit*/
            puts("User cancel signal received\n" );
            cleanup();
            puts("cleaned up and exit program \n");
            exit(0);
            break;

       default:
            break;
    }
}



void vmem_init(void){
    key_t shm_key = SHMKEY;
    int shm_id = VOID_IDX;
    shm_id = shmget(shm_key, SHMSIZE, IPC_CREAT | 0664);
    if( shm_id == VOID_IDX ){
        perror("(vmem_init) | Error initialising shmget");
        exit(EXIT_FAILURE);
    }
    void *ptr = shmat(shm_id, NULL, 0);
    if( ptr == (void *)VOID_IDX ){
        perror("(vmem_init) | Error initialising shmat");
        exit(EXIT_FAILURE);
    }
    vmem = (struct vmem_struct *)ptr;

    vmem->adm.size           = VMEM_VIRTMEMSIZE;
    vmem->adm.mmanage_pid    = getpid();
    vmem->adm.shm_id         = shm_id;
    vmem->adm.req_pageno     = VOID_IDX;
    vmem->adm.next_alloc_idx = VOID_IDX;
    vmem->adm.pf_count       = 0;
    vmem->adm.g_count        = 0;
    if( sem_init(&vmem->adm.sema, 1, 0) ){
        perror("sem_init() failed");
        exit(EXIT_FAILURE);
    }
    memset(vmem->adm.bitmap, 0, sizeof(vmem->adm.bitmap[0]) * VMEM_BMSIZE);
    int i;
    for( i = 0; i < VMEM_NPAGES; ++i ){
        vmem->pt.entries[i].flags   = 0;
        vmem->pt.entries[i].frame   = VOID_IDX;
        vmem->pt.entries[i].count = 0;
    }
    memset(vmem->pt.framepage, VOID_IDX, sizeof(vmem->pt.framepage[0]) * VMEM_NFRAMES);
    memset(vmem->data, 0, sizeof(vmem->data[0]) * VMEM_NFRAMES * VMEM_PAGESIZE);
}


void init_pagefile(const char *pfname){

    pagefile = fopen(pfname, PAGEFILE_OPEN_MODE);
    if( !pagefile ){
        perror("Error creating paging file\n");
        exit(EXIT_FAILURE);
    }
    srand(SEED);
    int i;
    int randvalue;
    for( i = 0; i < VMEM_VIRTMEMSIZE; ++i ){
        randvalue = rand();
        if( fwrite(&randvalue, sizeof(int), 1, pagefile) != 1){
            perror(" Error fwrite()\n");
            exit(EXIT_FAILURE);
        }
    }
    if(fflush(pagefile) == EOF){
        perror("Error fflush()\n");
        exit(EXIT_FAILURE);
    }
}

int find_remove_frame(void){
    int remove_frame = VOID_IDX;
    switch (VMEM_ALGO){
           case VMEM_ALGO_AGING:
                remove_frame = find_remove_aging();
                break;
           case VMEM_ALGO_CLOCK:
                remove_frame = find_remove_clock();
                break;
           case VMEM_ALGO_FIFO:
                remove_frame = find_remove_fifo();
                break;
           default:
                puts("no algorhithm chosen");
                break;
    }
    return remove_frame;
}



int find_remove_fifo(void){
    return vmem->adm.next_alloc_idx;
}

int find_remove_clock(void){
        int frame=vmem->adm.next_alloc_idx;
	int page = vmem->pt.framepage[frame];
	while((page!= VOID_IDX)&&((vmem->pt.entries[page].flags&(PTF_REF))== PTF_REF)){
		vmem->pt.entries[page].flags = vmem->pt.entries[page].flags & ~(PTF_REF);
		frame = (frame+1) % VMEM_NFRAMES;
		page = vmem->pt.framepage[frame];
	}
	vmem->adm.next_alloc_idx = frame;
	return frame;
}

int find_remove_aging(void){
    int i = vmem->adm.next_alloc_idx;
    for(i = 0; i < VMEM_NFRAMES; ++i){
        vmem->pt.entries[vmem->pt.framepage[i]].count >>= 1;
    }
    // frame mit niedrigsten counter suchen
    int lowest_count = vmem->pt.entries[vmem->pt.framepage[0]].count;
    int frame = 0;
    for(i = 1; i < VMEM_NFRAMES; ++i){
        if(vmem->pt.entries[vmem->pt.framepage[i]].count <= lowest_count){
            lowest_count = vmem->pt.entries[vmem->pt.framepage[i]].count;
            frame = i;
        }
    }
    vmem->adm.next_alloc_idx = frame;
    return frame;
}



int search_bitmap(void){
    int i;
    int free_bit = VOID_IDX;
    for(i = 0; i < VMEM_BMSIZE; i++){
        Bmword bitmap = vmem->adm.bitmap[i];
        Bmword mask = (i == (VMEM_BMSIZE - 1) ? VMEM_LASTBMMASK : 0);
        free_bit = find_free_bit(bitmap, mask);
        if(free_bit != VOID_IDX){
            free_bit = free_bit + i * VMEM_BITS_PER_BMWORD;
            break;
        }
    }
    return free_bit;
}


int find_free_bit(Bmword bmword, Bmword mask){
    int bit = VOID_IDX;
    Bmword bitmask = 1;
    bmword |= mask;
    for(bit = 0; bit < VMEM_BITS_PER_BMWORD; bit++){
        if(!(bmword & bitmask)){
            break;
        }
        bitmask <<= 1; 
    } 
    return bit < VMEM_BITS_PER_BMWORD ? bit : VOID_IDX;
}






void allocate_page(void){
    int req_pageno = vmem->adm.req_pageno;
    int frame = VOID_IDX;
    int page_removed_idx = VOID_IDX;

    // Prüfen, ob angeforderte Page geladen ist
    if(vmem->pt.entries[req_pageno].flags & PTF_PRESENT){
        return; // Abbrechen, da Page bereits geladen
    }
    // ...sonst
    // Bitmap nach freiem Frame durchsuchen
    frame = search_bitmap();
    if(frame != VOID_IDX){
        //fprintf(stderr, "Found free frame %d, ... allocating page\n", frame);
    }
    else{
        frame = find_remove_frame();
        page_removed_idx = vmem->pt.framepage[frame];
        if(vmem->pt.entries[page_removed_idx].flags & PTF_DIRTY){
            store_page(page_removed_idx); 
        }
        vmem->pt.entries[page_removed_idx].flags &= ~PTF_PRESENT;
    }
    update_pt(frame);
    fetch_page(vmem->adm.req_pageno);
    vmem->adm.pf_count++;
    struct logevent le;
    le.req_pageno = vmem->adm.req_pageno;
    le.replaced_page = page_removed_idx;
    le.alloc_frame = frame;
    le.g_count = vmem->adm.g_count;
    le.pf_count = vmem->adm.pf_count;
    logger(le);
    sem_post(&(vmem->adm.sema));
}


void cleanup(){
    if (fclose(pagefile)){
        perror("Error closing paging file");
        exit(EXIT_FAILURE);
    }

    if (fclose(logfile)){
        perror("Error closing logfile");
        exit(EXIT_FAILURE);
    }
    int shm_id = vmem->adm.shm_id;
    shmctl(shm_id, IPC_RMID, NULL);
}

void store_page(int pt_idx){
    int offset = pt_idx * sizeof(vmem->data[0]) * VMEM_PAGESIZE;
    int frame = vmem->pt.entries[pt_idx].frame;
    int *pstart = &vmem->data[frame * VMEM_PAGESIZE];
    if(fseek(pagefile, offset, SEEK_SET) == VOID_IDX){
        perror("Error in paging file ");
        exit(EXIT_FAILURE);
    }
    if(!fwrite(pstart, sizeof(vmem->data[0]),VMEM_PAGESIZE, pagefile )){
        perror("Error writing in paging file ");
        exit(EXIT_FAILURE);
    }
    fflush(pagefile);
}


void fetch_page(int pt_idx){
    int offset = pt_idx * sizeof(vmem->data[0]) * VMEM_PAGESIZE;
    int frame = vmem->pt.entries[pt_idx].frame;
    int *dest = &vmem->data[frame * VMEM_PAGESIZE];
    if(fseek(pagefile, offset, SEEK_SET) == -1){
        perror("Error in paging file ");
        exit(EXIT_FAILURE);
    }
    fread(dest, sizeof(vmem->data[0]), VMEM_PAGESIZE, pagefile);
}


void update_pt(int frame){
    int page_idx = vmem->adm.req_pageno;
    int bm_idx = frame / VMEM_BITS_PER_BMWORD;
    int bit = frame % VMEM_BITS_PER_BMWORD;
    vmem->adm.bitmap[bm_idx] |= (1U << bit);
    vmem->adm.next_alloc_idx = (vmem->adm.next_alloc_idx + 1) % VMEM_NFRAMES;
    vmem->pt.framepage[frame] = page_idx;
    vmem->pt.entries[page_idx].flags |= PTF_REF | PTF_PRESENT;
    vmem->pt.entries[page_idx].flags &= ~PTF_DIRTY;
    vmem->pt.entries[page_idx].frame = frame;
    vmem->pt.entries[page_idx].count = vmem->adm.g_count;
}


/* Do not change!  */
void
logger(struct logevent le)
{
    fprintf(logfile, "Page fault %10d, Global count %10d:\n"
            "Removed: %10d, Allocated: %10d, Frame: %10d\n",
            le.pf_count, le.g_count,
            le.replaced_page, le.req_pageno, le.alloc_frame);
    fflush(logfile);
}

