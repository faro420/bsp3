CC = gcc
CFLAGS = -g -Wall --std=gnu89
LFLAGS = -pthread
#override CFLAGS += -DDEBUG_MESSAGES

all: mmanage vmappl

mmanage: mmanage.h vmem.h mmanage.c
	$(CC) $(CFLAGS) $(LFLAGS) mmanage.c -o mmanage

vmappl: vmem.h vmappl.c vmappl.h vmaccess.h vmaccess.c
	$(CC) $(CFLAGS) $(LFLAGS) vmappl.c vmaccess.c -o vmappl
	
target1:
	./mmanage&
	
target2:
	./vmappl

clean:
	rm -f vmappl mmanage

remove:
	rm -f logfile.txt
