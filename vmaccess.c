
#include "vmaccess.h"
#include "vmem.h"


int phys_index(int address, int write);
struct vmem_struct *vmem = NULL;

void vm_init(void){
    key_t shm_key = SHMKEY;
    int shm_id = VOID_IDX;

    shm_id = shmget(shm_key, SHMSIZE, 0664);
    if(shm_id == VOID_IDX){
        perror("Error initialising shmget");
        exit(EXIT_FAILURE);
    }

    vmem = shmat(shm_id, NULL, 0);
    if(!vmem){
        perror("Error initialising shmat");
        exit(EXIT_FAILURE);
    }
}

int vmem_read(int address){
    int phys_addr = phys_index(address,0);
    return vmem->data[phys_addr];
}


void vmem_write(int address, int data){
    int phys_addr = phys_index(address,1);
    vmem->data[phys_addr] = data;
}

int phys_index(int address, int write){
    if(vmem == NULL){
        vm_init();
    }
    int page = address / VMEM_PAGESIZE;
    int offset = address % VMEM_PAGESIZE;
    int flags = vmem->pt.entries[page].flags;
    int req_page_is_loaded = ((flags & PTF_PRESENT) == PTF_PRESENT);
    if(!req_page_is_loaded){
        vmem->adm.req_pageno = page;
        kill(vmem->adm.mmanage_pid, SIGUSR1);
        sem_wait(&vmem->adm.sema); 
    }
    vmem->pt.entries[page].flags |= PTF_REF;
    if(write){
        vmem->pt.entries[page].flags |= PTF_DIRTY;
    }
    vmem->adm.g_count++;
    vmem->pt.entries[page].count = vmem->adm.g_count;
    return (vmem->pt.entries[page].frame * VMEM_PAGESIZE) + offset;
}

